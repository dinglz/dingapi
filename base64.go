package dingapi

import (
	"encoding/base64"
)

func Base64Encode(value string) string {
	return base64.StdEncoding.EncodeToString([]byte(value))
}

func Base64Decode(value string) string {
	res, _ := base64.StdEncoding.DecodeString(value)
	return string(res)
}
