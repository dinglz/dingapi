package dingapi

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/buger/jsonparser"
	"github.com/gookit/color"
)

func Translate(word string) string {
	var endres string
	var cnt int = 0
	endres = "\n"
	//把这个单词url编码了，因为可能有中文，也可能有空格
	word = url.QueryEscape(word)
	//上网查单词的时候遇到了这个网站，中英文各种语言都能翻译，为了方便学习(狗头)，我做了这样一个简单的程序把对应的单词数据爬下来
	res, _ := http.Get("http://www.iciba.com/word?w=" + word)
	defer res.Body.Close()
	doc, _ := goquery.NewDocumentFromReader(res.Body)
	//通过goquery库把这个底部用来传数据的json取出来
	res_str := doc.Find("#__NEXT_DATA__").Text()
	resthan, _, _, err := jsonparser.Get([]byte(res_str), "props", "pageProps", "initialReduxState", "word", "wordInfo", "baesInfo", "symbols")
	if err != nil {
		//没有这个路径，没有对应词汇
		color.Redln("翻译失败！未找到该单词")
		return "翻译失败"
	}
	jsonparser.ArrayEach(resthan, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		jsonparser.ArrayEach(value, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
			jsonparser.ArrayEach(value, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
				//层层遍历，取出翻译好的结果
				endres += strconv.Itoa(cnt+1) + "." + string(value) + "\n"
				cnt++
			}, "means")
		}, "parts")
	})
	//替换掉其中用来保存<>的unicode字符
	endres = strings.ReplaceAll(endres, "\\u003c", "<")
	endres = strings.ReplaceAll(endres, "\\u003e", ">")
	//把结果输出出来
	return endres
}
